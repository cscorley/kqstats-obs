let pkgs = import <nixpkgs> { };

in pkgs.mkShell rec {
  name = "kqstats";

  NODE_OPTIONS = "--openssl-legacy-provider";
  buildInputs = with pkgs; [ nodejs_20 ];
}
